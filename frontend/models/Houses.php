<?php
/**
 * Created by PhpStorm.
 * User: hiennq
 * Date: 5/4/16
 * Time: 14:28
 */

namespace frontend\models;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Houses extends \common\models\Houses
{
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
            'owner' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['user_id'],
                ],
                'value' => Yii::$app->user->identity->id,
            ],
        ];
    }
}