<?php
/**
 * Created by PhpStorm.
 * User: hiennq
 * Date: 5/4/16
 * Time: 14:32
 */

namespace frontend\models;
use Yii;
use yii\behaviors\TimestampBehavior;

class Users extends \common\models\User
{
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }
}