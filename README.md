# Requirement
- [XAMPP] (https://www.apachefriends.org/download.html) (only for run on local)
- PHP 5.4,MYSQL 5.5
- [composer](https://getcomposer.org/download/)

# Clone project
```
git clone git@bitbucket.org:ict58isd20152/realestate-web.git
```

# Update library
`composer global require "fxp/composer-asset-plugin:~1.1.1"`

```
composer update
```

# Config
## Setup Yii 2
```
php init.
After that, choose development to use the local database
```
## Create database 
```
http://localhost/phpmyadmin
Create new database name: realestate
```

## Migrate database
```
php yii migrate
```
## Config virtual host
### Config `httpd.config` (Need for enable virtual host).
Go to `\xampp\apache\conf\httpd.conf (windows)` or `/Applications/XAMPP/etc/httpd.conf (MAc os)`. 
Change that line 173,174 : 
```
User hiennq(Your user name)
Group admin(Default for mac os or group sudo if you are linux)
```
Uncomment or insert this line: 
```
# Virtual hosts
Include etc/extra/httpd-vhosts.conf
```
### Config `httpd-vhosts.conf` to enable virtual host
Go to `\xampp\apache\conf\extra\httpd-vhosts.conf (windows)` or `/Applications/XAMPP/etc/extra/httpd-vhosts.conf (MAc os)`
Open `httpd-vhosts.conf`, make sure you have this line, or it was uncomment `NameVirtualHost *:80`.
Add this code to the end of file httpd-vhosts.conf . 
```
<VirtualHost *:80>
    ServerName realestate.local
    DocumentRoot "/Applications/XAMPP/xamppfiles/htdocs/realestate-web"
    <Directory "/Applications/XAMPP/xamppfiles/htdocs/realestate-web">
        Options Indexes FollowSymLinks Includes execCGI
        AllowOverride All
        Order Allow,Deny
        Allow From All
    </Directory>
</VirtualHost>
```
### Config the hosts file
Add this line to the end of file `/etc/hosts`
```
127.0.0.1 realestate.local
```
Restart apache and goto `http://realestate.local` for frontend or `http://realestate.local/backend` for backend.