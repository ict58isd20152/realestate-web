<?php

namespace backend\models;
use Yii;

use yii\behaviors\TimestampBehavior;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

class Rooms extends \common\models\Rooms
{
    public static function findById($id){
        return Rooms::find()->where(['id'=>$id])->one();
    }
    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'owner' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['type'],
                ],
                'value' => 'room',
            ],
        ];
    }
    
    public function setData($data){
        $this->user_id=$data['user_id'];
        $this->price=$data['price'];
        $this->latitude=$data['latitude'];
        $this->longitude=$data['longitude'];
        $this->location=$data['location'];
        $this->area=$data['area'];
        $this->description=$data['description'];
        $this->rest_room=$data['rest_room'];
    }

    public static function getList($filter,$page,$pageSize){
        $page = $page > 1 ? $page : 1;
        $offset = (int)(($page - 1) * $pageSize);
        switch ($filter) {
            case "rate":
                return Houses::find()
                    ->orderBy(['(rate_point/rate_count)' => SORT_DESC])
                    ->limit($pageSize)
                    ->offset($offset)
                    ->all();
                break;
            case "newest":
                return Houses::find()
                    ->orderBy(['created_at' => SORT_DESC])
                    ->limit($pageSize)
                    ->offset($offset)
                    ->all();
                break;
            case "price":

                break;
            case "nearby":

                break;
            default:
                return Houses::find()
                    ->limit($pageSize)
                    ->offset($offset)
                    ->all();
                break;
        }
    }
}
