<?php
/**
 * Created by PhpStorm.
 * User: hiennq
 * Date: 4/30/16
 * Time: 15:16
 */

namespace backend\models;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

class User extends \common\models\User
{

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'owner' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['auth_key'],
                ],
                'value' => "",
            ],
        ];
    }

    public function setData($data){
        $this->setPassword($data['password']);
        $this->username=$data['email'];
        $this->email=$data['email'];
    }

    public static function checkLogin($username,$password){
        $user = User::find()->where(['username'=>$username])->one();
        if (!$user) return false;
        return Yii::$app->security->validatePassword($password, $user->password_hash);
    }
    
    public static function findByUsername($username){
        return User::find()->where(['username'=>$username])->one();
    }

    /**
     * @param $email
     * @return User|null|
     */
    public static function findByEmail($email){
        return User::find()->where(['email'=>$email])->one();
    }
    
}
