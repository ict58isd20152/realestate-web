<?php
/**
 * Created by PhpStorm.
 * User: hiennq
 * Date: 5/14/16
 * Time: 15:41
 */

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

class Authorize extends \console\models\Authorize
{
    public static function validateToken($token){
        $model = Authorize::find()->where(['token'=>$token])->one();
        return !is_null($model);
    }
}