<?php
/**
 * Created by PhpStorm.
 * User: hiennq
 * Date: 5/1/16
 * Time: 14:21
 */

namespace backend\models;

use yii\behaviors\TimestampBehavior;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

class Houses extends \common\models\Houses
{
    public static function findById($id){
        return Houses::find()->where(['id'=>$id])->one();
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'owner' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['type'],
                ],
                'value' => 'house',
            ],
        ];
    }

    public function setData($data){
        $this->user_id=$data['user_id'];
        $this->price=$data['price'];
        $this->latitude=$data['latitude'];
        $this->longitude=$data['longitude'];
        $this->location=$data['location'];
        $this->area=$data['area'];
        $this->description=$data['description'];
        $this->no_room=$data['no_room'];
        $this->no_rest_room=$data['no_rest_room'];
    }
    public static function getList($filter,$page,$pageSize){
        $page = $page > 1 ? $page : 1;
        $offset = (int)(($page - 1) * $pageSize);
        switch ($filter) {
            case "rate":
                return Houses::find()
                    ->orderBy(['(rate_point/rate_count)' => SORT_DESC])
                    ->limit($pageSize)
                    ->offset($offset)
                    ->all();
                break;
            case "newest":
                return Houses::find()
                    ->orderBy(['created_at' => SORT_DESC])
                    ->limit($pageSize)
                    ->offset($offset)
                    ->all();
                break;
            case "price":

                break;
            case "nearby":

                break;
            default:
                return Houses::find()
                    ->limit($pageSize)
                    ->offset($offset)
                    ->all();
                break;
        }
    }
}