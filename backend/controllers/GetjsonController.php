<?php

namespace backend\controllers;

use Yii;
use backend\models\User;
use backend\models\Rooms;
use backend\models\Houses;
use backend\models\Apartments;
use yii\helpers\Json;
use backend\models\Authorize;

class GetjsonController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionUsers ($username = NULL, $id=null,$token) {
		if(Authorize::validateToken($token)==false){echo "error";die();}
       	if ($username == NULL && $id == null) echo Json::encode(User::find()->all());
         else if ($username == NULL && $id != NULL) echo Json::encode(User::findId($id));
         else if ($username !=null && $id == null) echo Json::encode(User::findByUsername($username));
     }


    public function actionRooms ($id = NULL,$token) {
		if(Authorize::validateToken($token)==false){echo "error";die();}
    	if ($id == NULL) {
    		echo Json::encode(Rooms::find()->all());
    	} else {
    		echo Json::encode(Rooms::findById($id));
    	}	
    }

	public function actionHouses ($id = NULL,$token) {
		if(Authorize::validateToken($token)==false){echo "error";die();}
		if ($id == NULL) {
			echo Json::encode(Houses::find()->all());
		} else {
			echo Json::encode(Houses::findById($id));
		}
	}

	public function actionApartments ($id = NULL,$token) {
		if(Authorize::validateToken($token)==false){echo "error";die();}
		if ($id == NULL) {
			echo Json::encode(Apartments::find()->all());
		} else {
			echo Json::encode(Apartments::findById($id));
		}
	}

	public function actionProperty($token){
		if(Authorize::validateToken($token)==false){echo "error";die();}
		$rooms = Rooms::find()->all();
		$apartments = Apartments::find()->all();
		$houses = Houses::find()->all();
		echo Json::encode($this->parsePropertyData($rooms,$apartments,$houses));
	}

	//type: apartments, houses, rooms
	//filter: rate, newest, (nearby, price: coding )
	public function actionList_property($type,$filter='rate',$pageSize=10,$page=1,$token){
		if(Authorize::validateToken($token)==false){echo "error";die();}
		switch ($type){
			case "apartments":
				echo Json::encode(Apartments::getList($filter,$page,$pageSize));
				break;
			case "houses":
				echo Json::encode(Houses::getList($filter,$page,$pageSize));
				break;
			case "rooms":
				echo Json::encode(Rooms::getList($filter,$page,$pageSize));
				break;
		}
	}

	private function parsePropertyData($rooms,$apartments,$houses){
		return array_merge($rooms,$apartments,$houses);
	}

	public function actionGet_user_by_email($email,$token){
		if(Authorize::validateToken($token)==false){echo "error";die();}
		$user = User::find()->where(['email'=>$email])->one();
		return Json::encode($user);
	}
}
