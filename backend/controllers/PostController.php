<?php
/**
 * Created by PhpStorm.
 * User: hiennq
 * Date: 4/30/16
 * Time: 15:08
 */

namespace backend\controllers;

use backend\models\Apartments;
use backend\models\Rooms;
use backend\models\Houses;
use backend\models\User;
use backend\models\Authorize;
use Yii;

class PostController extends \yii\web\Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionRental($token)
    {
        if(Authorize::validateToken($token)==false){echo "error";die();}
        if(Yii::$app->request->post()){
            $model=null;
            $postData = Yii::$app->request->post();
            switch ($postData['type']){
                case 'apartment':
                    $model = new Apartments();
                    break;
                case 'room':
                    $model = new Rooms();
                    break;
                case 'house':
                    $model = new Houses();
                    break;
            }
            $model->setData($postData);
            if($model->validate()){
                if($model->save()) {
                    echo "Successfully Registered";
                }else{
                    echo "Could not register";
                }
            }else{
                echo "Could not validate data";
            }
        }
        else{
            echo "post data error";
        }
    }
    public function actionRegister($token)
    {
        if(Authorize::validateToken($token)==false){echo "error";die();}
        if(Yii::$app->request->post()){
            $model = new User();
            $postData = Yii::$app->request->post();
            $model->setData($postData);
            if($model->validate()){
                if($model->save()) {
                    echo "success";
                }else{
                    echo "error";
                }
            }else{
                echo "error";
            }
        }
        else{
            echo "error";
        }
    }
//    public function actionRegistertest($email,$pass)
//    {
//        $postData=[
//            'email'=>$email,
//            'password'=>$pass,
//        ];
//        var_dump($postData);die();
//            $model = new User();
//            $model->setData($postData);
//            if($model->validate()){
//                if($model->save()) {
//                    echo "Successfully Registered";
//                }else{
//                    echo "Could not register";
//                }
//            }else{
//                echo "Could not validate data";
//            }
//
//    }
    public function actionLogin($token)
    {
        if(Authorize::validateToken($token)==false){echo "error";die();}
        if(Yii::$app->request->post()){
            $postData = Yii::$app->request->post();
            $model = User::findByEmail($postData['email']);
            if(is_null($model)){
                echo "new";
                die();
            }
            if($model->validatePassword($postData['password'])){
                echo "success";
                die();
            }else{
                echo "wrong";
                die();
            }
        }
        else{
            echo "error";
            die();
        }
    }
}