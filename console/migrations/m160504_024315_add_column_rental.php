<?php

use yii\db\Migration;

class m160504_024315_add_column_rental extends Migration
{
    public function up()
    {
        $this->addColumn('{{%apartments}}','type','varchar(10) AFTER description');
        $this->addColumn('{{%rooms}}','type','varchar(10) AFTER description');
        $this->addColumn('{{%houses}}','type','varchar(10) AFTER description');
    }

    public function down()
    {
        $this->dropColumn('{{%apartments}}','type');
        $this->dropColumn('{{%rooms}}','type');
        $this->dropColumn('{{%houses}}','type');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
