<?php

use yii\db\Migration;

class m160507_084457_add_column_rate extends Migration
{
    public function up()
    {
        $this->addColumn('{{%apartments}}','rate_count','int AFTER longitude');
        $this->addColumn('{{%apartments}}','rate_point','int AFTER longitude');
        $this->addColumn('{{%houses}}','rate_count','int AFTER longitude');
        $this->addColumn('{{%houses}}','rate_point','int AFTER longitude');
        $this->addColumn('{{%rooms}}','rate_count','int AFTER longitude');
        $this->addColumn('{{%rooms}}','rate_point','int AFTER longitude');
    }

    public function down()
    {
        $this->dropColumn('{{%apartments}}','rate_count');
        $this->dropColumn('{{%apartments}}','rate_point');
        $this->dropColumn('{{%houses}}','rate_count');
        $this->dropColumn('{{%houses}}','rate_point');
        $this->dropColumn('{{%rooms}}','rate_count');
        $this->dropColumn('{{%rooms}}','rate_point');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
