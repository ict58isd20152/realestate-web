<?php

use yii\db\Migration;

class m160317_091518_edit_table_user extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->addColumn('{{%users}}','profile_id','int(11) AFTER id');
        $this->addColumn('{{%users}}','plan_id','int(11) AFTER id');
        $this->addColumn('{{%users}}','role','tinyint(1) AFTER status');
        $this->addColumn('{{%users}}','avatar','varchar(500) AFTER status');
        $this->addColumn('{{%users}}','full_name','varchar(100) AFTER status');
        $this->addColumn('{{%users}}','phone_number','varchar(15) AFTER status');
    }

    public function down()
    {
        $this->dropColumn('{{%users}}','role');
        $this->dropColumn('{{%users}}','full_name');
        $this->dropColumn('{{%users}}','phone_number');
        $this->dropColumn('{{%users}}','plan_id');
        $this->dropColumn('{{%users}}','profile_id');
        $this->dropColumn('{{%users}}','avatar');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
