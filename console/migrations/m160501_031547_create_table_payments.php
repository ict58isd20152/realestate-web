<?php

use yii\db\Migration;

class m160501_031547_create_table_payments extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('payments',[
            'id'=>'pk',
            'user_id'=>'int not null',
            'plan_id'=>'int not null',
            'transaction_id' => 'int',
            'quantity'=>'int not null',
            'description'=>'text',
            'created_at'=>'int not null',
            'updated_at'=>'int not null',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('payments');
    }
}
