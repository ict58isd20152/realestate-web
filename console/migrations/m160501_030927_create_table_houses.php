<?php

use yii\db\Migration;

class m160501_030927_create_table_houses extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('houses',[
            'id'=>'pk',
            'user_id'=>'int not null',
            'price'=>'float not null',
            'location'=>'text',
            'latitude'=>'float not null',
            'longitude'=>'float not null',
            'area'=>'float not null',
            'no_room'=>'int not null',
            'no_rest_room'=>'int not null',
            'description'=>'text',
            'created_at'=>'int not null',
            'updated_at'=>'int not null',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('houses');
    }
}
