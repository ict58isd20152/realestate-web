<?php

use yii\db\Migration;

class m160503_073001_add_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%rooms}}','photo2','text AFTER area');
        $this->addColumn('{{%rooms}}','photo1','text AFTER area');
        $this->addColumn('{{%rooms}}','currency','varchar(15) AFTER price');
        $this->addColumn('{{%apartments}}','photo2','text AFTER area');
        $this->addColumn('{{%apartments}}','photo1','text AFTER area');
        $this->addColumn('{{%apartments}}','currency','varchar(15) AFTER price');
        $this->addColumn('{{%houses}}','photo2','text AFTER area');
        $this->addColumn('{{%houses}}','photo1','text AFTER area');
        $this->addColumn('{{%houses}}','currency','varchar(15) AFTER price');
        $this->addColumn('{{%users}}','address','varchar(200) AFTER email');
    }

    public function down()
    {
        $this->dropColumn('{{%rooms}}','photo2');
        $this->dropColumn('{{%rooms}}','photo1');
        $this->dropColumn('{{%rooms}}','currency');
        $this->dropColumn('{{%apartments}}','photo2');
        $this->dropColumn('{{%apartments}}','photo1');
        $this->dropColumn('{{%apartments}}','currency');
        $this->dropColumn('{{%houses}}','photo2');
        $this->dropColumn('{{%houses}}','photo1');
        $this->dropColumn('{{%houses}}','currency');
        $this->dropColumn('{{%users}}','address');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
