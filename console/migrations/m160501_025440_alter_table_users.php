<?php

use yii\db\Migration;

class m160501_025440_alter_table_users extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->addColumn('{{%users}}','customer_object','varchar(15) AFTER plan_id');
    }

    public function down()
    {
        $this->dropColumn('{{%users}}','customer_object');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
