<?php

use yii\db\Migration;

class m160501_031401_create_table_plans extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('plans',[
            'id'=>'pk',
            'price'=>'float not null',
            'currency'=>'varchar(15)',
            'description'=>'text',
            'created_at'=>'int not null',
            'updated_at'=>'int not null',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('plans');
    }
}
