<?php

use yii\db\Migration;

class m160501_032434_create_foreign_key extends Migration
{
    public function up()
    {
        $this->addForeignKey('fk_user_room','{{%rooms}}','user_id','users','id','RESTRICT','CASCADE');
        $this->addForeignKey('fk_user_plan_id','{{%users}}','plan_id','plans','id','RESTRICT','CASCADE');
        $this->addForeignKey('fk_payment_plan_id','{{%payments}}','plan_id','plans','id','RESTRICT','CASCADE');
        $this->addForeignKey('fk_payment_user_id','{{%payments}}','user_id','users','id','RESTRICT','CASCADE');
        $this->addForeignKey('fk_user_apartment','{{%apartments}}','user_id','users','id','RESTRICT','CASCADE');
        $this->addForeignKey('fk_user_house','{{%houses}}','user_id','users','id','RESTRICT','CASCADE');
    }

    public function down()
    {
        echo "m160501_032434_foreign_key cannot be reverted.\n";

        return false;
    }
}
