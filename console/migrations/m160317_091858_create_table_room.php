<?php

use yii\db\Migration;

class m160317_091858_create_table_room extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {      
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('rooms',[
            'id'=>$this->primaryKey(),
            'user_id'=>'int not null',
            'latitude'=>'float not null',
            'longitude'=>'float not null',
            'description'=>'text',
            'price'=>'float not null',
            'area'=>'float',
            'rest_room'=>'boolean',
            'created_at'=>'int not null',
            'updated_at'=>'int not null',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('rooms');
    }
}
