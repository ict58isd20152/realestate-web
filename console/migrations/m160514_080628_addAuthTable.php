<?php

use yii\db\Migration;

class m160514_080628_addAuthTable extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('authorize',[
            'id'=>'pk',
            'token_name'=>'text',
            'token'=>'varchar(32) not null',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('authorize');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
