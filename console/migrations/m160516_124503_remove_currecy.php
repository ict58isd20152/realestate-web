<?php

use yii\db\Migration;

class m160516_124503_remove_currecy extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%apartments}}','currency');
        $this->dropColumn('{{%houses}}','currency');
        $this->dropColumn('{{%rooms}}','currency');
        $this->dropColumn('{{%plans}}','currency');
    }

    public function down()
    {
        echo "m160516_124503_remove_currecy cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
