<?php
/**
 * Created by PhpStorm.
 * User: hiennq
 * Date: 5/14/16
 * Time: 15:26
 */

namespace console\controllers;
use console\models\Authorize;
use yii;
use yii\console\Controller;

class GenController extends Controller
{
    public function actionIndex()
    {
        echo "Gen token\n";
        echo "Command: php yii gen/create android -> For gen android token\n";
    }

    public function actionCreate($tokenName)
    {
        echo "Gen token: ".$tokenName."\n";
        $model = Authorize::findByName($tokenName);
        if($model==null){
            $model = new Authorize();
            $model->token_name = $tokenName;
            $model->token = Yii::$app->security->generateRandomString();
            if($model->save()){
                echo "Create token ".$tokenName." successfully\n";
                echo "Token ".$tokenName.": ".$model->token."\n";
            }else{
                echo "Create token fail\n";
            }
        }else{
            echo "Token name was exist !\n";
        }
    }
}