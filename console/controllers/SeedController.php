<?php
/**
 * Created by PhpStorm.
 * User: hiennq
 * Date: 5/1/16
 * Time: 11:09
 */

namespace console\controllers;
use yii;
use yii\console\Controller;

class SeedController extends Controller
{
    public function actionIndex()
    {
        echo "Seed database by Hien Nguyen\n";

        $seeder = new \tebazil\yii2seeder\Seeder();
        $generator = $seeder->getGeneratorConfigurator();
        $faker = $generator->getFakerConfigurator();

        $seeder->table('{{%plans}}')->columns([
            'id',
            'price'=>$faker->randomNumber(),
            'description'=>$faker->text(),
            'created_at'=>$faker->unixTime,
            'updated_at'=>$faker->unixTime
        ])->rowQuantity(100);

        $seeder->table('{{%users}}')->columns([
            'id',
            'plan_id'=>$faker->numberBetween(1,100),
            'email'=>$faker->unique()->email,
            'address'=>$faker->address,
            'role'=>'0',
            'status'=>'1',
            'username'=>$faker->unique()->userName,
            'password_hash'=>Yii::$app->security->generatePasswordHash("123456"),
            'full_name'=>$faker->name,
            'avatar'=>$faker->imageUrl(),
            'phone_number'=>$faker->phoneNumber,
            'created_at'=>$faker->unixTime,
            'updated_at'=>$faker->unixTime
        ])->rowQuantity(100);

        $seeder->table('{{%payments}}')->columns([
            'id',
            'user_id'=>$faker->numberBetween(1,100),
            'plan_id'=>$faker->numberBetween(1,100),
            'quantity'=>$faker->numberBetween(1,1000),
            'description'=>$faker->text(),
            'created_at'=>$faker->unixTime,
            'updated_at'=>$faker->unixTime
        ])->rowQuantity(100);

        $seeder->table('{{%apartments}}')->columns([
            'id',
            'user_id'=>$faker->numberBetween(1,100),
            'price'=>$faker->randomFloat(),
            'location'=>$faker->streetAddress,
            'latitude'=>$faker->latitude,
            'longitude'=>$faker->longitude,
            'rate_count'=>$faker->numberBetween(40,60),
            'rate_point'=>$faker->numberBetween(160,200),
            'area'=>$faker->randomFloat(2,0,100),
            'photo1'=>$faker->imageUrl(),
            'photo2'=>$faker->imageUrl(),
            'no_room'=>$faker->numberBetween(0,32),
            'no_rest_room'=>$faker->numberBetween(0,32),
            'description'=>$faker->text(),
            'type'=>'apartment',
            'created_at'=>$faker->unixTime,
            'updated_at'=>$faker->unixTime,
        ])->rowQuantity(100);

        $seeder->table('{{%rooms}}')->columns([
            'id',
            'user_id'=>$faker->numberBetween(1,100),
            'price'=>$faker->randomFloat(),
            'location'=>$faker->streetAddress,
            'latitude'=>$faker->latitude,
            'longitude'=>$faker->longitude,
            'rate_count'=>$faker->numberBetween(40,60),
            'rate_point'=>$faker->numberBetween(160,200),
            'area'=>$faker->randomFloat(2,0,100),
            'photo1'=>$faker->imageUrl(),
            'photo2'=>$faker->imageUrl(),
            'rest_room'=>$faker->boolean(),
            'description'=>$faker->text(),
            'type'=>'room',
            'created_at'=>$faker->unixTime,
            'updated_at'=>$faker->unixTime,
        ])->rowQuantity(100);

        $seeder->table('{{%houses}}')->columns([
            'id',
            'user_id'=>$faker->numberBetween(1,100),
            'price'=>$faker->randomFloat(),
            'location'=>$faker->streetAddress,
            'latitude'=>$faker->latitude,
            'longitude'=>$faker->longitude,
            'rate_count'=>$faker->numberBetween(40,60),
            'rate_point'=>$faker->numberBetween(160,200),
            'area'=>$faker->randomFloat(2,0,100),
            'photo1'=>$faker->imageUrl(),
            'photo2'=>$faker->imageUrl(),
            'no_room'=>$faker->numberBetween(0,32),
            'no_rest_room'=>$faker->numberBetween(0,32),
            'description'=>$faker->text(),
            'type'=>'house',
            'created_at'=>$faker->unixTime,
            'updated_at'=>$faker->unixTime,
        ])->rowQuantity(100);

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand('SET FOREIGN_KEY_CHECKS = 0');
        $command->query();
        $seeder->refill();
        $command = $connection->createCommand('SET FOREIGN_KEY_CHECKS = 1');
        $result = $command->query();

        echo "Seed database by Hien Nguyen\n";
    }
}