<?php
/**
 * Created by PhpStorm.
 * User: hiennq
 * Date: 5/14/16
 * Time: 15:41
 */

namespace console\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "authorize".
 *
 * @property integer $id
 * @property string $token_name
 * @property string $token
 */

class Authorize extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authorize';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['token_name'], 'string'],
            [['token'], 'required'],
            [['token'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'token_name' => 'Token Name',
            'token' => 'Token',
        ];
    }

    public static function findByName($tokenName){
        return Authorize::find()->where(['token_name'=>$tokenName])->one();
    }
}