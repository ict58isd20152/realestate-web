<?php
/**
 * Created by PhpStorm.
 * User: hiennq
 * Date: 5/1/16
 * Time: 14:15
 */

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%apartments}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property double $price
 * @property string $location
 * @property double $latitude
 * @property double $longitude
 * @property double $area
 * @property integer $no_room
 * @property integer $no_rest_room
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Users $user
 */
class Apartments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%apartments}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'price', 'latitude', 'longitude', 'area', 'no_room', 'no_rest_room'], 'required'],
            [['user_id', 'no_room', 'no_rest_room', 'created_at', 'updated_at'], 'integer'],
            [['price', 'latitude', 'longitude', 'area'], 'number'],
            [['location', 'description'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'price' => 'Price',
            'location' => 'Location',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'area' => 'Area',
            'no_room' => 'No Room',
            'no_rest_room' => 'No Rest Room',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return ApartmentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApartmentsQuery(get_called_class());
    }
}