<?php
/**
 * Created by PhpStorm.
 * User: hiennq
 * Date: 5/1/16
 * Time: 14:15
 */

namespace common\models;

/**
 * This is the ActiveQuery class for [[Apartments]].
 *
 * @see Apartments
 */
class ApartmentsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Apartments[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Apartments|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}