<?php
/**
 * Created by PhpStorm.
 * User: hiennq
 * Date: 5/1/16
 * Time: 14:19
 */

namespace common\models;

/**
 * This is the ActiveQuery class for [[Houses]].
 *
 * @see Houses
 */
class HousesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Houses[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Houses|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}