<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%rooms}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property double $latitude
 * @property double $longitude
 * @property string $description
 * @property double $price
 * @property double $area
 * @property string $location
 * @property integer $rest_room
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Users $user
 */
class Rooms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rooms}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'latitude', 'longitude', 'price'], 'required'],
            [['user_id', 'rest_room', 'created_at', 'updated_at'], 'integer'],
            [['latitude', 'longitude', 'price', 'area'], 'number'],
            [['description', 'location'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'description' => 'Description',
            'price' => 'Price',
            'area' => 'Area',
            'location' => 'Location',
            'rest_room' => 'Rest Room',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return RoomsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RoomsQuery(get_called_class());
    }

}